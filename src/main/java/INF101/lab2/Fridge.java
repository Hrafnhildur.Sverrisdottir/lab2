package INF101.lab2;

import java.util.ArrayList;
//import java.util.List;
//import java.util.HashSet;
import java.util.NoSuchElementException;
//import java.time.LocalDate;

class Fridge implements IFridge {

    int maxSize = 20;
    ArrayList<FridgeItem> myFridge = new ArrayList<FridgeItem>(); 


    @Override
    public boolean placeIn(FridgeItem item){
        if (nItemsInFridge() < totalSize()){
            myFridge.add(item);
            return true;
        }
        return false;
    }

    @Override
    public int totalSize(){
        return maxSize;
    }

    @Override
    public int nItemsInFridge(){
        return myFridge.size();
    }

    @Override
    public void takeOut(FridgeItem item){
        if (myFridge.contains(item)){
            myFridge.remove(item);
        }
        else throw new NoSuchElementException();
    }

    @Override
    public void emptyFridge(){
        myFridge.clear();
    }

    @Override
    public ArrayList<FridgeItem> removeExpiredFood(){
        ArrayList<FridgeItem> expiredItems = new ArrayList<FridgeItem>();

        for (int i = 0; i < myFridge.size(); i++){
            if (myFridge.get(i).hasExpired()){
                expiredItems.add(myFridge.get(i));
                myFridge.remove(myFridge.get(i));
                i--;
            }
        }
        return expiredItems;
    }


    

    


}
